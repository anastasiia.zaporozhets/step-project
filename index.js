const servicesTabsContainer = document.querySelector(".our-services-tabs");
const tabsTitle = document.querySelectorAll(".tabs-title");
const tabsItems = document.querySelectorAll(".tabs-item");

servicesTabsContainer.addEventListener("click", function (event) {
    if (event.target.classList.contains("tabs-title")) {
        const currentTitle = event.target;
        const tabId = currentTitle.getAttribute("data-tab");
        const contentTab = document.querySelector(tabId);

        tabsTitle.forEach(function (item) {
            item.classList.remove("active");
        });

        tabsItems.forEach(function (item) {
            item.classList.remove("active");
        });

        currentTitle.classList.add("active");
        contentTab.classList.add("active");
    }
});

const imagesGallery = [
    { src: "/img/graphic%20design/graphic-design1.jpg", alt: "graphic-design", category: "graphic" },
    { src: "/img/graphic%20design/graphic-design2.jpg", alt: "graphic-design", category: "graphic" },
    { src: "/img/graphic%20design/graphic-design3.jpg", alt: 'graphic-design', category: "graphic" },
    { src: "/img/web%20design/web-design1.jpg", alt: "web-design", category: "web" },
    { src: "/img/web%20design/web-design2.jpg", alt: "web-design", category: "web" },
    { src: "/img/web%20design/web-design3.jpg", alt: "web-design", category: "web" },
    { src: "/img/landing%20page/landing-page1.jpg", alt: "landing-page", category: "landing" },
    { src: "/img/landing%20page/landing-page2.jpg", alt: "landing-page", category: "landing" },
    { src: "/img/landing%20page/landing-page7.jpg", alt: "landing-page", category: "landing" },
    { src: "/img/wordpress/wordpress5.jpg", alt: "wordpress", category: "wordpress" },
    { src: "/img/wordpress/wordpress6.jpg", alt: "wordpress", category: "wordpress" },
    { src: "/img/wordpress/wordpress4.jpg", alt: "wordpress", category: "wordpress" }
];

const galleryContainer = document.querySelector('.work-gallery-img');
const loadMoreButton = document.querySelector('.btn-work-gallery');

let imagesAdditional = [
    { src: "/img/graphic%20design/graphic-design4.jpg", alt: "graphic-design", category: "graphic" },
    { src: "/img/graphic%20design/graphic-design5.jpg", alt: "graphic-design", category: "graphic" },
    { src: "/img/graphic%20design/graphic-design6.jpg", alt: "graphic-design", category: "graphic" },
    { src: "/img/web%20design/web-design5.jpg", alt: "web-design", category: "web" },
    { src: "/img/web%20design/web-design6.jpg", alt: "web-design", category: "web" },
    { src: "/img/web%20design/web-design7.jpg", alt: "web-design", category: "web" },
    { src: "/img/landing%20page/landing-page4.jpg", alt: "landing-page", category: "landing" },
    { src: "/img/landing%20page/landing-page5.jpg", alt: "landing-page", category: "landing" },
    { src: "/img/landing%20page/landing-page6.jpg", alt: "landing-page", category: "landing" },
    { src: "/img/wordpress/wordpress8.jpg", alt: "wordpress", category: "wordpress" },
    { src: "/img/wordpress/wordpress9.jpg", alt: "wordpress", category: "wordpress" },
    { src: "/img/wordpress/wordpress10.jpg", alt: "wordpress", category: "wordpress" }
];


function createImageContainer(image) {
    const imageContainer = document.createElement("div");
    imageContainer.className = "image-container";

    const link = document.createElement("a");
    link.href = "#";

    const img = document.createElement("img");
    img.className = "work-img";
    img.src = image.src;
    img.alt = image.alt;

    const blocCreativeDesign = document.createElement("div");
    blocCreativeDesign.className = "bloc-creative-design";

    const hoverLine = document.createElement("div");
    hoverLine.className = "hover-line";

    const iconCreativeDesign = document.createElement("img");
    iconCreativeDesign.className = "icon-creative-design";
    iconCreativeDesign.src = "img/img-hover/icon.png";

    const blocTextTitle = document.createElement("p");
    blocTextTitle.className = "bloc-text-title";
    blocTextTitle.innerText = "creative design";

    const blocText = document.createElement("p");
    blocText.className = "bloc-text";
    blocText.innerText = image.category;

    blocCreativeDesign.appendChild(hoverLine);
    blocCreativeDesign.appendChild(iconCreativeDesign);
    blocCreativeDesign.appendChild(blocTextTitle);
    blocCreativeDesign.appendChild(blocText);

    link.appendChild(img);
    link.appendChild(blocCreativeDesign);

    imageContainer.appendChild(link);

    imageContainer.addEventListener("mouseover", function () {
        blocText.style.display = "block";
    });

    imageContainer.addEventListener("mouseout", function () {
        blocText.style.display = "none";
    });

    return imageContainer;
}


function updateGallery(images) {
    galleryContainer.innerHTML = "";
    images.forEach(image => {
        const imageContainer = createImageContainer(image);
        galleryContainer.appendChild(imageContainer);
    });
}

function loadMoreImages() {
    updateGallery([...imagesGallery, ...imagesAdditional]);
    loadMoreButton.style.display = "none";
}

function filterImages(category) {
    let filteredImages;

    if (category === "all") {
        filteredImages = imagesGallery.slice(0, 12);
        loadMoreButton.style.display = "block";
    } else {
        filteredImages = [...imagesGallery, ...imagesAdditional].filter(image => image.category === category);
        if (filteredImages.length > 24) {
            filteredImages = filteredImages.slice(0, 24);
        }
        loadMoreButton.style.display = "none";
    }

    updateGallery(filteredImages);
}

const tabTitles = document.querySelectorAll(".tabs-work-title");

tabTitles.forEach(tabTitle => {
    tabTitle.addEventListener("click", function () {
        tabTitles.forEach(tab => {
            tab.classList.remove("active-tabs");
        });

        this.classList.add("active-tabs");

        const category = this.dataset.category;

        filterImages(category);
    });
});

loadMoreButton.addEventListener("click", loadMoreImages);

filterImages("all");




$(document).ready(function() {
    $(".slider-for").on("afterChange", function(event, slick, currentSlide) {
        const activeSlideIndex = currentSlide;

        const textSliderContent = $(".text-slider-content[data-index='" + activeSlideIndex + "']");

        $(".text-slider-content").removeClass("active");
        textSliderContent.addClass("active");
    });



    $(".slider-for").slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        speed: 500,
        arrows: false,
        fade: true,
        asNavFor: ".slider-nav"
    });
    $(".slider-nav").slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        speed: 500,
        asNavFor: ".slider-for",
        dots: true,
        centerMode: true,
        focusOnSelect: true
    });
});










